import React from "react";
import ReactDOM from "react-dom";

import Header from "./Header";
import "./index.css";

const App = () => (
  <div>
    <Header />
    <div>This is served from header app</div>
  </div>
);

ReactDOM.render(<App />, document.getElementById("app"));
