import React from "react";

export default () => (
  <div
    style={{
      padding: "1em",
      margin: "1em",
      background: "green",
      color: "black",
      fontWeight: "bold",
    }}
  >
    Header 1.0
  </div>
);
